<h1>Compra Rápida APP Entregador</h1>

Para rodar o projeto é necessario possuir um emulador android

**Aplicativo para entregadores**

**Primeira etapa** do aplicativo é o acesso com login e senha.

Exemplo:

Login: **teste@email.com**

Senha: **teste123**


Após realizar o acesso com login e senha corretamente, abrirá a tela principal (home) 
nela é possível abrir a sessão de Menu onde terá acesso à alguns dados como:

- Mensagens
- Pagamentos
- Listas
- Contato

Na **segunda etapa** ainda na tela principal teremos então as ultímas listas que foram geradas.

Em listas temos:

- Produtos
- Marca
- Quantidade


Em pedidos temos:

- Quantidade de itens
- Nome do supermercado
- Data da realização da compra
- Status do pedido
- Chat
- Tempo do pedido

Na nossa **terceira etapa** Ao aceitar fazer a entrega será aberto o mapa com a rota ate o supermercado e ate o destino do cliente.

**Quarta etapa** Ao chegar ao super mercado o botão central habilitara para mudar o status para "em compras"

Apos fazer as compras e cegar perto da localização de destino o botão habilitará novamente para finalizar a entrega.

[APK para download](https://gitlab.com/projeto-de-desenvolvimento/compra-rapido/app-shopper/-/blob/50e9f829a3f16de9859c5a7b09e789f6f4c0a4d4/app-release.apk)
